# OpenML dataset: dgf_96f4164d-956d-4c1c-b161-68724eb0ccdc

https://www.openml.org/d/43028

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Arbres urbains

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43028) of an [OpenML dataset](https://www.openml.org/d/43028). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43028/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43028/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43028/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

